<?php

namespace FAE\logging;

use Monolog\Logger as monologger;
use Psr\Log\LoggerInterface;
use Monolog\Handler\ErrorLogHandler;

/**
 * Methods to manage logging.
 * 
 * Example usage: logger::log()->info('This is a log message');
 */
class logger {
  
  /**
   * Registry channels, with their registered handlers.
   * @var type 
   */
  private static $_logs = []; 
  
  /**
   * Define a default set of "sensible" loggers (default the standard error log).
   */
  public static function defaultLogHandlers() {
    
    foreach ([
        // Log handlers are in stack order, with last being the _first_ in the stack.
        new ErrorLogHandler() 
    ] as $handler) {
      yield $handler;
    }
    
  }
  
  /**
   * Register a log type
   * @param string $channel Channel to use
   * @param array $handlers An array of handlers for the default Logger
   * @param Logger $logger Optional logger replacement
   */
  public static function register(string $channel, array $handlers = [], ?monologger $logger = null) {
    
    if (empty($logger)) {
      $logger = new monologger($channel);
    }

    if (!empty($handlers)) {
      if (is_callable([$logger, 'setHandlers'])) {
        $logger->setHandlers($handlers);
      }
    }
    
    if (empty($logger->getHandlers())) {
      foreach (self::defaultLogHandlers() as $handler) {
        $logger->pushHandler($handler);
      }
    }
    self::$_logs[$channel] = $logger;
  }
  
  /**
   * Retrieve a logging object 
   * @param string $channel The previously registered log handler, or blank for the default channel
   * @return \Psr\Log\LoggerInterface
   * @throws \RuntimeException
   */
  public static function log(string $channel = 'fae'): LoggerInterface {
    
    if (empty(self::$_logs[$channel])) {

      // No logger registered for this, so create one with defaults.
      self::register($channel);

    }
    
    return self::$_logs[$channel];
  }
  
  
}
