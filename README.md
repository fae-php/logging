# Logging interface

This is a simple backport of the ARIA logging interface to FAE.

## Example basic usage

```
use FAE\logging\logger;

...

logger::log()->debug('This is a test with the fae log object!');

```

## Example using a new channel

Channels allow you to namespace error messages for e.g. a specific subsystem.

```

logger::log('subsystem')->debug('This is a test with the fae log object!');

```


## Example using special logger

The default log interface is to output to the PHP error log. You may want to specify a channel that outputs somewhere else - a slack channel or similar.

For this you want to include an array of one or more handlers, that will be passed to the default Monolog `Logger` interface.


```
logger::register('customloggerchannel', [
    new CustomHandler()
]);

logger::log('customloggerchannel')->debug('This is a test with the fae log object!');

```
